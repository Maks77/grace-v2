$(document).ready(function () {
	
	let menuIsOpen = false;

	$('.menu-trigger').click(function(e) {
		$(this).toggleClass('menu-btn-active');
		$('.main-nav__list-holder').toggleClass('open');
	})

	$(window).on('click', function (e) {
		if (
			!$('.main-nav__list-holder').hasClass('open') &&
			!$('.menu-trigger').hasClass('menu-btn-active') ) {
			// console.log('nothing')
		} else {
			if ($(e.target).hasClass('menu-trigger') || $(e.target).parents().hasClass('menu-trigger')) {
				// console.log('nothing2')
			} else {
				if ($(e.target).parents().hasClass('main-nav__list-holder') ||
					$(e.target).hasClass('main-nav__list-holder')) {
					// console.log('nothing3')
				} else {
					console.log('shut the door')
					$('.menu-trigger').toggleClass('menu-btn-active');
					$('.main-nav__list-holder').toggleClass('open');
				}	
			}
		}
	})

	let isPhoneDevice = "ontouchstart" in document.documentElement;
	

	let itemWidth = $('.product-item').width();
	$('.product-item').height(itemWidth);
	$(window).resize(function () {
		itemWidth = $('.product-item').width();
		$('.product-item').height(itemWidth);
	})

	if (isPhoneDevice) return;

	$('.product-item').on('mouseover', function (e) {

		let prodItem = $(this).find('.product-item__img');
		let bgBlock = $(this).find('.product-item__img');

		if ($(e.target).hasClass('anim100')) {
			return;
		}

		$(prodItem).removeClass('anim100');
		$(bgBlock).removeClass('bg-anim100');

		$(prodItem).addClass('anim50');
		$(bgBlock).addClass('bg-anim50');
	})

	$('.product-item').on('mouseleave', function (e) {
		let prodItem = $(this).find('.product-item__img');
		let bgBlock = $(this).find('.product-item__img');

		$(prodItem).addClass('anim100');
		$(bgBlock).addClass('bg-anim100');

		$(prodItem).removeClass('anim50');
		$(bgBlock).removeClass('bg-anim50');
	})

	

})